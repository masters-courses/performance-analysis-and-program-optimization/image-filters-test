#include <string>
#include <vector>
#include <memory>

// Base filters
#include "filter/base/base.hpp"
#include "filter/base/matrix_base.hpp"

// Linear filters
#include "filter/linear/brightness.hpp"
#include "filter/linear/glass.hpp"
#include "filter/linear/gray-world.hpp"
#include "filter/linear/grayscale.hpp"
#include "filter/linear/invert.hpp"
#include "filter/linear/median.hpp"
#include "filter/linear/perfect-reflector.hpp"
#include "filter/linear/rotate.hpp"
#include "filter/linear/sepia.hpp"
#include "filter/linear/transfer.hpp"
#include "filter/linear/waves_x.hpp"
#include "filter/linear/waves_y.hpp"

// Matrix filters
#include "filter/matrix/blur.hpp"
#include "filter/matrix/embossing.hpp"
#include "filter/matrix/gaussian_blur.hpp"
#include "filter/matrix/harshness.hpp"
#include "filter/matrix/motion-blur.hpp"
#include "filter/matrix/pruitt_x.hpp"
#include "filter/matrix/pruitt_y.hpp"
#include "filter/matrix/scharr_x.hpp"
#include "filter/matrix/scharr_y.hpp"
#include "filter/matrix/sharpness.hpp"
#include "filter/matrix/sobel_x.hpp"
#include "filter/matrix/sobel_y.hpp"

const QString path_to_256 = "";
const QString path_to_512 = "";
const QString path_to_1024 = "";
const QString path_to_2048 = "";

int main()
{
	QImage currentImage;
	if (currentImage.load(path_to_2048)) {
		std::vector<std::unique_ptr<BaseFilter>> filters;
		filters.reserve(24);

		// Linear filters
		filters.emplace_back(std::make_unique<BrightnessFilter>());
		filters.emplace_back(std::make_unique<GlassFilter>());
		filters.emplace_back(std::make_unique<GrayWorldFilter>());
		filters.emplace_back(std::make_unique<GrayScaleFilter>());
		filters.emplace_back(std::make_unique<InvertFilter>());
		filters.emplace_back(std::make_unique<MedianFilter>());
		filters.emplace_back(std::make_unique<PerfectReflectorFilter>());
		filters.emplace_back(std::make_unique<RotateFilter>(60));
		filters.emplace_back(std::make_unique<SepiaFilter>());
		filters.emplace_back(std::make_unique<TransferFilter>(300));
		filters.emplace_back(std::make_unique<Waves_X>());
		filters.emplace_back(std::make_unique<Waves_Y>());

		// Matrix filters
		filters.emplace_back(std::make_unique<BlurFilter>());
		filters.emplace_back(std::make_unique<EmbossingFilter>());
		filters.emplace_back(std::make_unique<GaussianBlurFilter>());
		filters.emplace_back(std::make_unique<HarshnessFilter>());
		filters.emplace_back(std::make_unique<MotionBlurFilter>());
		filters.emplace_back(std::make_unique<Pruitt_X>());
		filters.emplace_back(std::make_unique<Pruitt_Y>());
		filters.emplace_back(std::make_unique<Scharr_X>());
		filters.emplace_back(std::make_unique<Scharr_Y>());
		filters.emplace_back(std::make_unique<SharpnessFilter>());
		filters.emplace_back(std::make_unique<Sobel_X>());
		filters.emplace_back(std::make_unique<Sobel_Y>());

		#pragma omp parallel for
		for (int i = 0; i < filters.size(); i++) {
			if (i == 2 || i == 6) {
				if (i == 2) {
					GrayWorldFilter* filter = dynamic_cast<GrayWorldFilter*>(filters[i].get());
					filter->calculateFields(currentImage);
				}
				else {
					PerfectReflectorFilter* filter = dynamic_cast<PerfectReflectorFilter*>(filters[i].get());
					filter->calculateFields(currentImage);
				}
			}

			filters[i]->processImage(currentImage).save("./images/" + filters[i]->getName() + ".png");;
		}
	}
	return 0;
}
