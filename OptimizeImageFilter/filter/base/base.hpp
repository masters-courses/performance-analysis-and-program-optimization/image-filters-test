#ifndef BASEFILTER_HPP
#define BASEFILTER_HPP

#include <QImage>

class BaseFilter {
protected:
    virtual QColor calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const = 0;
public:
    virtual ~BaseFilter() = default;
    virtual QImage processImage(const QImage& image) const;

    virtual QString getName() = 0;
};

QImage BaseFilter::processImage(const QImage& image) const {
    int width = image.width();
    int height = image.height();
    QVector<QRgb> buffer(width * height);

    #pragma omp parallel for
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            QColor calculatedColor = calculateNewPixelColor(image, x, y);
            buffer[y * width + x] = calculatedColor.rgba();
        }
    }

    return QImage(reinterpret_cast<uchar*>(buffer.data()), width, height, QImage::Format_ARGB32);
}

// Шаблонная функция для ограничения значений
template <typename T>
T clamp(T value, T min,T max) {
    if (value > max) return max;
    else if (value < min) return min;
    return value;
}

#endif // BASEFILTER_HPP
