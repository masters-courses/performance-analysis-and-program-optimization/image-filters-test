#ifndef MATRIXBASEFILTER_HPP
#define MATRIXBASEFILTER_HPP

#include "base.hpp"

class MatrixBaseFilter : public BaseFilter {
protected:
	float* arrayOfData;
	int radiusCore;
public:
	MatrixBaseFilter(const int radius);
	MatrixBaseFilter(const int radius, float* array);
	~MatrixBaseFilter();

	QColor calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const override;

	QString getName() override;
};


MatrixBaseFilter::MatrixBaseFilter(const int radius) : radiusCore(radius) {
	int size = 2 * radiusCore + 1;
	arrayOfData = new float[size * size];
}

MatrixBaseFilter::MatrixBaseFilter(const int radius, float* array) : radiusCore(radius) {
	int size = 2 * radiusCore + 1;
	arrayOfData = new float[size * size];

	for (int i = 0; i < size * size; i++) {
		arrayOfData[i] = array[i];
	}

	delete array;
}

MatrixBaseFilter::~MatrixBaseFilter() {
	if (arrayOfData) {
		delete[] arrayOfData;
	}
}

QColor MatrixBaseFilter::calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const {
	float returnR{ 0 };
	float returnG{ 0 };
	float returnB{ 0 };

	int size = 2 * radiusCore + 1;

	for (int i = -radiusCore; i <= radiusCore; i++) {
		for (int j = -radiusCore; j <= radiusCore; j++) {
			int index = (i + radiusCore) * size + j + radiusCore;

			QColor color = image.pixelColor(clamp<int>(coordinateX + j, 0, image.width() - 1),
				clamp<int>(coordinateY + i, 0, image.height() - 1));

			returnR += color.red() * arrayOfData[index];
			returnG += color.green() * arrayOfData[index];
			returnB += color.blue() * arrayOfData[index];

		}
	}

	return QColor(clamp<int>(returnR, 0, 255),
		clamp<int>(returnG, 0, 255),
		clamp<int>(returnB, 0, 255));

}

QString MatrixBaseFilter::getName() {
	return QString("Matrix Filter");
}

#endif // MATRIXBASEFILTER_HPP
