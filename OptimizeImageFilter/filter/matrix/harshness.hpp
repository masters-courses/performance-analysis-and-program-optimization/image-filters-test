#ifndef HARSHNESSFILTER_HPP
#define HARSHNESSFILTER_HPP

#include "../base/matrix_base.hpp"

class HarshnessFilter : public MatrixBaseFilter {
public:
    HarshnessFilter();

    QString getName() override;
};

HarshnessFilter::HarshnessFilter() : MatrixBaseFilter(1, new float[9]{-1,-1,-1,-1,9,-1,-1,-1,-1}) {

}

QString HarshnessFilter::getName() {
    return QString("Harshness");
}

#endif // HARSHNESSFILTER_HPP
