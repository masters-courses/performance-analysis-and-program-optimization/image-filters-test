#ifndef GAUSSIANBLURFILTER_HPP
#define GAUSSIANBLURFILTER_HPP

#include <math.h>

#include "../base/matrix_base.hpp"

class GaussianBlurFilter : public MatrixBaseFilter {
public :
    GaussianBlurFilter(const int radius = 3);

    void createGaussianVector(const int sigma);

    QString getName() override;
};

GaussianBlurFilter::GaussianBlurFilter(const int radius) : MatrixBaseFilter(radius) {
    createGaussianVector(2);
}

void GaussianBlurFilter::createGaussianVector(const int sigma) {
    int size = 2 * radiusCore + 1;
    float normalization = 0;

    for (int i = -radiusCore; i <= radiusCore; i++) {
        for (int j = -radiusCore; j <= radiusCore; j++) {
            int index = (i + radiusCore) * size + (j + radiusCore);
            arrayOfData[index] = exp(-(i * i + j * j) / (sigma * sigma));
            normalization += arrayOfData[index];
        }
    }

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++){
            arrayOfData[size*i + j] /= normalization;
        }
    }

}

QString GaussianBlurFilter::getName() {
    return QString("Gaussian blur");
}

#endif // GAUSSIANBLURFILTER_HPP
