#ifndef SHARPNESSFILTER_HPP
#define SHARPNESSFILTER_HPP

#include "../base/matrix_base.hpp"

class SharpnessFilter : public MatrixBaseFilter {
public:
    SharpnessFilter();

    QString getName() override;
};

SharpnessFilter::SharpnessFilter() : MatrixBaseFilter(1, new float[9]{0,-1,0,-1,5,-1,0,-1,0}) {

}

QString SharpnessFilter::getName() {
    return QString("Sharpness");
}

#endif // SHARPNESSFILTER_HPP
