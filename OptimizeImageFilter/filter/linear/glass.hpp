#ifndef GLASSFILTER_HPP
#define GLASSFILTER_HPP

#include <cstdlib>

#include "../base/base.hpp"

class GlassFilter : public BaseFilter {
public:
    QColor calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const override;

    QString getName() override;
};

QColor GlassFilter::calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const {
    int coordinateNewX = coordinateX + ( (rand()%10)/10.f - 0.5 ) * 10;
    int coordinateNewY = coordinateY + ( (rand()%10)/10.f - 0.5 ) * 10;

    return image.pixelColor( clamp<int>(coordinateNewX, 0, image.width()-1 ), clamp<int>(coordinateNewY, 0, image.height()-1 ));
}

QString GlassFilter::getName() {
    return QString("Glass");
}

#endif // GLASSFILTER_HPP
