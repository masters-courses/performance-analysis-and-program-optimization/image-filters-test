#ifndef INVERTFILTER_HPP
#define INVERTFILTER_HPP

#include "../base/base.hpp"

class InvertFilter : public BaseFilter {
public:
    QColor calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const override;

    QString getName();
};

QColor InvertFilter::calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const {
    QColor resultColor = image.pixelColor(coordinateX,coordinateY);
    resultColor.setRgb(255 - resultColor.red(),
                       255 - resultColor.green(),
                       255 - resultColor.blue() );

    return resultColor;
}

QString InvertFilter::getName() {
    return QString("Invert");
}

#endif // INVERTFILTER_HPP
