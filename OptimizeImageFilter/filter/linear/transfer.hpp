#ifndef TRANSFERFILTER_HPP
#define TRANSFERFILTER_HPP

#include "../base/base.hpp"

class TransferFilter : public BaseFilter {
    int transfer;
public:
    TransferFilter(const int _transfer = 50);

    QColor calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const override;

    QString getName() override;
};

TransferFilter::TransferFilter(const int _transfer ) : transfer(_transfer) {

}

QColor TransferFilter::calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const {
    QColor resultColor = QColor(0,0,0);
    if ( image.width()-transfer > coordinateX ) {
        resultColor = image.pixelColor(coordinateX+transfer, coordinateY);
    }

    return resultColor;
}

QString TransferFilter::getName() {
    return QString("Transfer");
}

#endif // TRANSFERFILTER_HPP
