#ifndef MEDIANFILTER_HPP
#define MEDIANFILTER_HPP

#include <algorithm>

#include "../base/base.hpp"

class MedianFilter : public BaseFilter {
    int radiusCore;
public:
    MedianFilter(int _radiusCore = 1);
    QColor calculateNewPixelColor (const QImage& image, int coordinateX, int coordinateY) const override;

    QString getName() override;
};

MedianFilter::MedianFilter(int _radiusCore) : radiusCore(_radiusCore) {

}

QColor MedianFilter::calculateNewPixelColor (const QImage& image, int coordinateX, int coordinateY) const {
    int sizeOfArray = 2 * radiusCore + 1;

    int* returnR = new int[sizeOfArray*sizeOfArray];
    int* returnG = new int[sizeOfArray*sizeOfArray];
    int* returnB = new int[sizeOfArray*sizeOfArray];

    int index = 0;

    for(int i = -radiusCore; i <= radiusCore; i++) {
        for(int j = -radiusCore; j <= radiusCore; j++) {
            QColor color = image.pixelColor( clamp<int>( coordinateX+j, 0, image.width() - 1 ),
                                             clamp<int>( coordinateY+i, 0, image.height() - 1 ) );

            returnR[index] = color.red();
            returnG[index] = color.green();
            returnB[index] = color.blue();

            index++;
        }
    }

    std::sort(returnR, returnR+sizeOfArray*sizeOfArray);
    std::sort(returnG, returnG+sizeOfArray*sizeOfArray);
    std::sort(returnB, returnB+sizeOfArray*sizeOfArray);

    QColor result = QColor( clamp<int>(returnR[(sizeOfArray*sizeOfArray-1)/2], 0, 255),
            clamp<int>(returnG[(sizeOfArray*sizeOfArray-1)/2], 0, 255),
            clamp<int>(returnB[(sizeOfArray*sizeOfArray-1)/2], 0, 255) );

    delete[] returnR;
    delete[] returnG;
    delete[] returnB;

    return result;
}

QString MedianFilter::getName() {
    return QString("Median");
}

#endif // MEDIANFILTER_HPP
