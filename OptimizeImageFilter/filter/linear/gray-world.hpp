#ifndef GRAYWORLDFILTER_HPP
#define GRAYWORLDFILTER_HPP

#include "../base/base.hpp"

class GrayWorldFilter : public BaseFilter {
    double middleRed;
    double middleGreen;
    double middleBlue;

    double avgMiddleRGB;

public :
    GrayWorldFilter();

    void calculateFields(const QImage& image);

    QColor calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const override;

    QString getName() override;
};


GrayWorldFilter::GrayWorldFilter() : middleRed(0), middleGreen(0), middleBlue(0), avgMiddleRGB(0) {

}

void GrayWorldFilter::calculateFields(const QImage& image) {

    for (int x = 0; x < image.width(); x++) {
        for (int y = 0; y < image.height(); y++) {
            QColor color = image.pixelColor(x, y);

            middleRed += color.red();
            middleGreen += color.green();
            middleBlue += color.blue();
        }
    }

    middleRed /= image.width() * image.height();
    middleGreen /= image.width() * image.height();
    middleBlue /= image.width() * image.height();

    avgMiddleRGB = (middleRed + middleGreen + middleBlue) / 3;

}

QColor GrayWorldFilter::calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const {

    QColor resultColor = image.pixelColor( coordinateX, coordinateY );
    resultColor.setRgb( clamp<int>(resultColor.red() * avgMiddleRGB / middleRed, 0, 255),
                        clamp<int>(resultColor.green() * avgMiddleRGB / middleGreen, 0, 255),
                        clamp<int>(resultColor.blue() * avgMiddleRGB / middleBlue, 0, 255) );

    return resultColor;
}

QString GrayWorldFilter::getName() {
    return QString("Gray World");
}

#endif // GRAYWORLDFILTER_HPP
